<?  
	//inicia o banco de dados
	require 'bd.php';

	//monta o tema
    require 'tema/cabecalho.php';
    require 'tema/lateral.php';

    //pegando o nome da categoria
    $query = "SELECT `name` FROM `categorias` WHERE `idCategoria` = " . $_GET['cat'];
    $resultado = mysqli_query($dbhandle, $query);
    $categoria = mysqli_fetch_array($resultado, MYSQLI_ASSOC);

	//pegando a data da ultima pesquisa feita
    $query = "SELECT MAX(data) FROM pesquisa WHERE `idCategoria` = " . $_GET['cat'];
    //echo $query;
    $resultado = mysqli_query($dbhandle, $query);
    $pesquisa = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
    $pesquisa = $pesquisa["MAX(data)"];

    //Pegando id da ultima pesquisa feita
    echo "ultima pesquisa: " . $pesquisa;
    $query = "SELECT `id` FROM pesquisa WHERE `idCategoria` = " . $_GET['cat'] . " AND `data` = '" . $pesquisa . "'";
    //echo $query;
    $resultado = mysqli_query($dbhandle, $query);
    $pesquisa = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
    $pesquisa = $pesquisa['id'];

    //pegando os produtos da categoria
    $query = "SELECT * FROM `resultados` WHERE `pesquisa_id` = " . $pesquisa;
    $produtos = mysqli_query($dbhandle, $query);
?>
            
<h1>Categoria: <?echo $categoria['name']?></h1>
<!-- início da tabela -->
<div class="col-xs-12">
	<div class="box">
	    
	    <div class="box-body table-responsive">
	        <table id="example2" class="table table-bordered table-hover">
	            <thead>
	                <tr>
	                    <th>Produto</th>
	                    <th>Quantidade de Vendas</th>
	                    <th>Preço</th>
	                </tr>
	            </thead>
	            <tbody>
	            	<?//inserindo dados na tabela
	            		while($row = mysqli_fetch_array($produtos, MYSQLI_ASSOC)) {
	            			echo "<tr><td><a href=" . $row['url'] . " target=_blank>" . $row['title'] . "</a></td>";
	            			echo "<td>" . $row['sold_quantity'] . "</td>";
	            			echo "<td>R$ " . $row['price'] . "</td></tr>";
	            		}
	            	?>
	            </tbody>
	        </table>
	    </div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
<!-- fim da tabela -->
<? require 'tema/rodape.php';?>





# Vendas no Mercado Livre



## Overview

Ferramenta para listar os produtos mais vendidos de cada categoria do Mercado Livre


####  Instruções

1. Instalar o banco de dados utilizando o arquivo "phpMyAdmin.sql" pelo phpMyAdmin ou "banco.mwb" pelo MysqlWorkbench; 
2. Preencher os dados de acesso do Banco de Dados nos aquivos "bd.php" e "cron.php";
3. Configurar o script "cron.php" nas tarefas cron do servidor e programar para ser executado a cada 5 minutos.


### Desenvolvido por

Fabricio Saab   
<fabricio.saab@me.com>  
<http://fabriciosaab.wordpress.com>
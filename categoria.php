<?  
	//inicia o banco de dados
	require 'bd.php';

	//monta o tema
    require 'tema/cabecalho.php';
    require 'tema/lateral.php';

    //pegando o nome da categoria
    $query = "SELECT `name` FROM `categorias` WHERE `idCategoria` = " . $_GET['cat'];
    $resultado = mysqli_query($dbhandle, $query);
    $categoria = mysqli_fetch_array($resultado, MYSQLI_ASSOC);

	//pegando a data da ultima pesquisa feita
    $query = "SELECT MAX(data) FROM pesquisa WHERE `idCategoria` = " . $_GET['cat'];
    //echo $query;
    $resultado = mysqli_query($dbhandle, $query);
    $pesquisa = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
    $pesquisa = $pesquisa["MAX(data)"];

    //Pegando id da ultima pesquisa feita
    //echo "ultima pesquisa: " . $pesquisa;
    $query = "SELECT `id` FROM pesquisa WHERE `idCategoria` = " . $_GET['cat'] . " AND `data` = '" . $pesquisa . "'";
    //echo $query;
    $resultado = mysqli_query($dbhandle, $query);
    $pesquisa = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
    $pesquisa = $pesquisa['id'];
    //var_dump($pesquisa);

    //pegando os produtos da categoria
    $query = "SELECT * FROM `resultados` WHERE `pesquisa_id` = " . $pesquisa;
    //echo $query;
    $produtos = mysqli_query($dbhandle, $query);

    //pegando as categorias filhas
    $query = "SELECT * FROM `categorias` WHERE `idPai` = '" . $_GET['cat'] . "'";
    $resultado = mysqli_query($dbhandle, $query);
    $categoriasFilhas[] = 0;
    while($row = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
        $categoriasFilhas[] = $row;
    }
    //var_dump($categoriasFilhas);

?>

            
 <h1>Categoria: <?echo $categoria['name']?></h1>
<!-- início da tabela -->
<div class="col-xs-12">
	<div class="box" style="padding: 0px 5px 0px 5px;}">
		<h4>Sub-Categorias</h4>
		<?//var_dump($categoriasFilhas);
			foreach ($categoriasFilhas as $filha) {
				echo "<a href= subcategoria.php?cat=" . $filha['idCategoria'] . ">" . $filha['name'] . "</a>";
				echo "<br>";
			}
		?>
		</div>
</div>
<? require 'tema/rodape.php';?>



